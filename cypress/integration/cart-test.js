describe('Test av handlekurven', function() {

    it('Kunde legger inn varer', function () {

        // Åpne nettsiden
		cy.visit('/');

        // Gjennomfør testen
        cy.get('#product').select('Hubba bubba');
        cy.get('#quantity').clear().type('4');
        cy.get('#saveItem').click();

        cy.get('#product').select('Smørbukk');
        cy.get('#quantity').clear().type('5');
        cy.get('#saveItem').click();

        cy.get('#product').select('Stratos');
        cy.get('#quantity').clear().type('1');
        cy.get('#saveItem').click();

        cy.get('#product').select('Hobby');
        cy.get('#quantity').clear().type('2');
        cy.get('#saveItem').click();

        // Verifiser resultatet
        cy.get('#price').should('have.text', '33');
	});

    it('Kunde sletter varer fra handlekurven', function () {

        // Åpne nettsiden
		cy.visit('/');

        // Gjennomfør testen
        cy.get('#product').select('Hubba bubba');
        cy.get('#quantity').clear().type('4');
        cy.get('#saveItem').click();

        cy.get('#product').select('Smørbukk');
        cy.get('#quantity').clear().type('5');
        cy.get('#saveItem').click();

        cy.get('#deleteItem').click();

        // Verifiser resultatet
        cy.get('#price').should('have.text', '8');
	});

    it('Kunde oppdaterer antall varer for et produkt', function () {

        // Åpne nettsiden
		cy.visit('/');

        // Gjennomfør testen
        cy.get('#product').select('Hubba bubba');
        cy.get('#quantity').clear().type('4');
        cy.get('#saveItem').click();

        cy.get('#product').select('Smørbukk');
        cy.get('#quantity').clear().type('5');
        cy.get('#saveItem').click();

        cy.get('#product').select('Hubba bubba');
        cy.get('#quantity').clear().type('3');
        cy.get('#saveItem').click();

        // Verifiser resultatet
        cy.get('#price').should('have.text', '11');
	})
});