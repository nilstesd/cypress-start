# Web-testing med cypress.js

Dette er en superenkel eksempelapplikasjon som er satt opp med cypress-testing. Applikasjonen server en enkel webside med express.js.

### Instruksjoner

1. Klon prosjektet:

```
git clone https://gitlab.stud.idi.ntnu.no/nilstesd/cypress-start.git
```

2. Installer avhengigheter med npm:

```
cd cypress-start
npm install
```

3. Kjør express-serveren med applikasjonen:

```
npm start
```

4. Sjekk at applikasjonen kjører:

[http://localhost:8080](http://localhost:8080)

5. Kjør cypress:

Åpne et nytt konsoll i prosjektmappen. 

```
npx cypress open
```

Hvis du får feilmelding som sier at cypress executable ikke er funnet kan du kjøre følgende før du prøver å starte cypress på nytt (jeg måtte det):

```
npx cypress install
```

(npx er inkludert med npm > v5.2)

6. Kjør testene

Dette gjøres fra applikasjonen som startes med `npx cypress open`.
